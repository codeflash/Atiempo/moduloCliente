package atiempo.codeflash.com.atiempoemisor;

import java.util.List;

import retrofit2.http.GET;
import retrofit2.Callback;

/**
 * Created by yazidakbar on 30/11/2016.
 */

public interface EmpresaServicio {

    @GET("/api/v1/ruta")
    void getEmpresa(Callback<List<Empresa>> callback);

}
