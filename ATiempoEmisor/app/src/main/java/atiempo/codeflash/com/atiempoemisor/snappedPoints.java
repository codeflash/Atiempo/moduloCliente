package atiempo.codeflash.com.atiempoemisor;

/**
 * Created by yazidakbar on 30/11/2016.
 */

public class snappedPoints {
    private int placeId;
    private location location;
    private int originalIndex;

    public atiempo.codeflash.com.atiempoemisor.location getLocation() {
        return location;
    }

    public int getOriginalIndex() {
        return originalIndex;
    }

    public void setPlaceId(int placeId) {
        this.placeId = placeId;
    }

    public void setOriginalIndex(int originalIndex) {
        this.originalIndex = originalIndex;
    }

    public void setLocation(atiempo.codeflash.com.atiempoemisor.location location) {
        this.location = location;
    }

    public int getPlaceId() {
        return placeId;
    }



}

