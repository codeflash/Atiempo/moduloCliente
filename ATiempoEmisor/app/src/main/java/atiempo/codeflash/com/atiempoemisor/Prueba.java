package atiempo.codeflash.com.atiempoemisor;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.List;

import retrofit2.*;


public class Prueba extends AppCompatActivity {
    TextView resul;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_prueba);
        resul=(TextView) findViewById(R.id.resultado);

        final Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://13.68.210.71")
                .build();
        EmpresaServicio servicio=retrofit.create(EmpresaServicio.class);
            servicio.getEmpresa(new Callback<List<Empresa>>() {
            @Override
            public void onResponse(Call<List<Empresa>> call, Response<List<Empresa>> response) {

            resul.setText(call.toString());

            }

            @Override
            public void onFailure(Call<List<Empresa>> call, Throwable t) {

            resul.setText(t.getMessage());

            }
        });



    }
}
