package atiempo.codeflash.com.atiempoemisor;

/**
 * Created by yazidakbar on 30/11/2016.
 */

public class Empresa {
    private int id;
    private String descripcion;
    private ruta ruta;

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public atiempo.codeflash.com.atiempoemisor.ruta getRuta() {
        return ruta;
    }

    public void setRuta(atiempo.codeflash.com.atiempoemisor.ruta ruta) {
        this.ruta = ruta;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "Empresa: "+ "id:"+id + "descripcion: "+descripcion+ "ruta: "+ ruta.getSnappedPoints().getPlaceId()+ruta.getSnappedPoints().getLocation().getLatitude()+ruta.getSnappedPoints().getLocation().getLongitude();

    }
}
